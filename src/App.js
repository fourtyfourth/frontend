import React, { useState, useRef } from 'react';
import axios from 'axios';
import Editor from '@monaco-editor/react';
import { FillSpinner as Loader } from 'react-spinners-kit';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';
import Typography from '@material-ui/core/Typography';

import examples from './examples';

const languages = [
  {
    name: 'Javascript',
    code: 1
  },
  {
    name: 'Python',
    code: 0
  }
];

const useStyles = makeStyles(theme => ({
  toolbar: {
    borderBottom: `1px solid ${theme.palette.divider}`
  },
  toolbarTitle: {
    flex: 1
  },
  toolbarSecondary: {
    justifyContent: 'space-between',
    overflowX: 'auto'
  },
  toolbarLink: {
    padding: theme.spacing(1),
    flexShrink: 0
  }
}));

function App() {
  const [theme, setTheme] = useState('dark');
  const [language, setLanguage] = useState('javascript');
  const [isEditorReady, setIsEditorReady] = useState(false);
  const valueGetter = useRef();
  const classes = useStyles();
  const title = 'Online compiler';

  const handleEditorDidMount = _valueGetter => {
    setIsEditorReady(true);
    valueGetter.current = _valueGetter;
  };

  const compileRequest = () => {
    const code = valueGetter.current();
    console.log(language);
    const codeLanguage = languages.find(el => {
      return el.name === language;
    });
    const stdin = '';

    axios
      .post('/compile', { language: codeLanguage.code, code, stdin })
      .then(response => {
        console.log(response);
      })
      .catch(e => {
        console.log(e);
      });
  };

  const toggleTheme = () => {
    setTheme(theme === 'light' ? 'dark' : 'light');
  };

  const toggleLanguage = () => {
    setLanguage(language === 'javascript' ? 'python' : 'javascript');
  };

  return (
    <>
      <React.Fragment>
        <Toolbar className={classes.toolbar}>
          <Button size="small">Subscribe</Button>
          <Typography
            component="h2"
            variant="h5"
            color="inherit"
            align="center"
            noWrap
            className={classes.toolbarTitle}
          >
            {title}
          </Typography>
          <IconButton>
            <SearchIcon />
          </IconButton>
          <Button variant="outlined" size="small">
            Sign up
          </Button>
        </Toolbar>
      </React.Fragment>
      <Button
        variant="contained"
        onClick={toggleTheme}
        disabled={!isEditorReady}
      >
        Toggle theme
      </Button>
      <Button
        variant="contained"
        onClick={toggleLanguage}
        disabled={!isEditorReady}
      >
        Toggle language
      </Button>
      <Button
        variant="contained"
        onClick={compileRequest}
        disabled={!isEditorReady}
      >
        Compile
      </Button>
      <div style={{ display: 'flex' }}>
        <Editor
          height="90vh" // By default, it fully fits with its parent
          theme={theme}
          language={language}
          loading={<Loader />}
          value={examples[language]}
          editorDidMount={handleEditorDidMount}
          options={{ lineNumbers: 'off' }}
        />
      </div>
    </>
  );
}

export default App;
